# Laravel Custom Rules


## Usage

in your Controller

use your custom rule by new [rulename] in your validation array.

## eg:
use Illuminate\Support\Facades\Validator;
use App\Rules\alphaspace;

$validator = Validator::make($request->all(), [
  'name' => ['required','string','max:30',new alphaspace],

  ]);



## Licence

GPLv3 only. See License.md
